mock-lock-demo
==============

Simple demo of an approach to using mocks to test a class that utilizes a
`java.util.concurrent.Lock`


Foo
---

`Foo` is a service bean that does something.  It utilizes a `Lock` to make sure
that it can't be asked to do something while it's already doing something.  When
it does something it notifies an injected `FooListener` that it did something.

The salient points of `Foo`

* The `Lock` is not intended to be injected into the bean.  It is a `final` 
  field in the class (as all good lock instances should be).
* It has a package-private constructor that takes a `Lock` as an argument.
* There is default no-arg constructor for `Foo`.  This is required by CDI in 
  order to instantiate the bean.  It simply calls the other constructor using
  the appropriate (real) `Lock` subclass.

FooTest
-------

The unit test for `Foo` makes use of the package-private constructor to inject
a mock `Lock` into an instance of `Foo`.  The test cases can then manipulate the
lock as needed to do path testing on the `doSomething` method.

> **NOTE**:
> It's tempting to try to write this test like this which _will not work_.
> ```
> @Mock
> private Lock lock;
>
> private Foo foo = new Foo(lock);
> ...
> ```
> 
> This doesn't work because the mock objects are not injected into `@Mock`
> annotated fields until *after* the instance of `FooTest` is constructed 
> and initialized, and thus after the initializer for `foo` has been evaluated.
> The result is that `foo` ends up having a `null` lock.