package ceh.demo;

import java.util.concurrent.locks.Lock;

import org.jmock.Expectations;
import org.jmock.auto.Mock;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class FooTest {

  @Rule
  public final JUnitRuleMockery context = new JUnitRuleMockery();

  @Mock
  private FooListener listener;

  @Mock
  private Lock lock;

  private Foo foo;

  @Before
  public void setUp() throws Exception {
    foo = new Foo(lock);
    foo.listener = listener;
  }

  @Test
  public void testWhenNotLocked() throws Exception {
    context.checking(new Expectations() {
      {
        oneOf(lock).tryLock();
        will(returnValue(true));
        oneOf(listener).didSomething();
        oneOf(lock).unlock();
      }
    });

    foo.doSomething();
  }

  @Test
  public void testWhenLocked() throws Exception {
    context.checking(new Expectations() {
      {
        try {
          oneOf(lock).tryLock();
          will(returnValue(false));
        }
        catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

    foo.doSomething();
  }


}
