package ceh.demo;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class Foo {

  private final Lock lock;

  @Inject
  protected FooListener listener;

  public Foo() {
    this(new ReentrantLock());
  }

  Foo(Lock lock) {
    this.lock = lock;
  }

  public void doSomething() {
    if (lock.tryLock()) {
      try {
        listener.didSomething();
      }
      finally {
        lock.unlock();
      }
    }
  }

}
